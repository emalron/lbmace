﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace LBMace
{
    class Setup
    {
        private static Setup setup;
        
        public int[] size, inletID, map;
        public double u0, viscosity, Re, tau;
        public Bitmap tiles;
        public double[] fin, fout, density, ux, uy;

        private Setup()
        {
            inletID = new int[2];
            size = new int[2];
            tau = 0.62d;
        }

        public static Setup get()
        {
            if(setup == null)
            {
                setup = new Setup();
            }
            return setup;
        }

        public void relaxationTime(double Re, double tau)
        {
            double inletLeng, x0, x1, y0, y1;
            x0 = inletID[0] % size[0];
            x1 = inletID[1] % size[0];
            y0 = inletID[0] / size[0];
            y1 = inletID[1] / size[0];

            inletLeng = Math.Abs(Math.Sqrt(Math.Pow((x1 - x0), 2) + Math.Pow((y1 - y0), 2)));
            this.Re = Re;

            // viscosity = 0.04d;
            // tau = 3d * viscosity + 0.5d;
            
            u0 = ((tau-0.5d)/3d) * this.Re / inletLeng;
        }

        public void mapping()
        {
            loadFile();
            size = sizing();
            map = getColor();
            inletID = counterColor(2);

            init();
        }

        private void init()
        {
            int area = size[0] * size[1];
            double[] weight = new double[9] { 4d / 9d, 1d / 9d, 1d / 9d, 1d / 9d, 1d / 9d, 1d / 36d, 1d / 36d, 1d / 36d, 1d / 36d };

            fin = new double[area * 9];
            fout = new double[area * 9];
            density = new double[area];
            ux = new double[area];
            uy = new double[area];

            for (int index = 0; index < area; index++)
            {
                density[index] = 1d;
                ux[index] = 0;
                uy[index] = 0;

                for(int n=0;n<9;n++)
                {
                    fin[n + 9 * index] = weight[n];
                    fout[n + 9 * index] = weight[n];
                }
            }
        }

        private void loadFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".bmp";
            ofd.Filter = "Map file (.bmp)|*.bmp";

            ofd.ShowDialog();

            if(ofd.FileName != "")
            {
                tiles = new Bitmap(Image.FromFile(ofd.FileName));
            }
        }

        private int[] sizing()
        {
            int[] lengs = new int[2];

            lengs[0] = tiles.Width;
            lengs[1] = tiles.Height;

            return lengs;
        }

        private int[] getColor()
        {
            int[] output = new int[size[0] * size[1]];
            int i, j;

            for (int index = 0; index < size[0] * size[1]; index++)
            {
                i = index % size[0];
                j = index / size[0];

                if (tiles.GetPixel(i, j) == Color.FromArgb(255, 0, 0, 0))
                {
                    output[index] = 1;
                }
                else if (tiles.GetPixel(i, j) == Color.FromArgb(255, 255, 255, 255))
                {
                    output[index] = 0;
                }
                else if (tiles.GetPixel(i, j) == Color.FromArgb(255, 255, 0, 0))
                {
                    output[index] = 2;
                }
                else if (tiles.GetPixel(i, j) == Color.FromArgb(255, 0, 0, 255))
                {
                    output[index] = 3;
                }
                else if (tiles.GetPixel(i, j) == Color.FromArgb(255, 0, 128, 0))
                {
                    output[index] = 4;
                }
            }

            return output;
        }

        private int[] counterColor(int target)
        {
            int[] output = new int[2];
            int counter;

            counter = 0;

            for (int index = 0; index < size[0] * size[1]; index++)
            {
                if (map[index] == target)
                {
                    if(counter == 0)
                    {
                        output[0] = index;
                    }
                    output[1] = index;
                    counter++;
                }
            }

            return output;
        }
    }
}
