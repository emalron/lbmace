﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloo;

namespace LBMace
{
    class GPGPU
    {
        Setup data;
        ComputeContext ctx;
        ComputeBuffer<int> gpu_map, gpu_lx, gpu_ly, gpu_number, gpu_number9;
        ComputeBuffer<double> gpu_u0, gpu_fin, gpu_fout, gpu_density, gpu_u, gpu_v, gpu_omega, gpu_lower, gpu_upper;
        ComputeCommandQueue cq;
        ComputeKernel kernelCollision;
        ComputeKernel kernelStream;
        ComputeKernel kernelSwap;
        ComputeKernel kernelBounceback;
        ComputeKernel kernelBC;
        ComputeKernel kernelMacro;
        ComputeProgram prog;
        long[] worker;
        long[] worker9;
        string kernels;

        public GPGPU()
        {
            data = Setup.get();
        }

        public void init() {
            createKernels();
            declareMeta();
            compileKernel();
        }

        private void declareMeta()
        {
            ComputeContextPropertyList properties = new ComputeContextPropertyList(ComputePlatform.Platforms[0]);
            ctx = new ComputeContext(ComputeDeviceTypes.Gpu, properties, null, IntPtr.Zero);
            cq = new ComputeCommandQueue(ctx, ctx.Devices[0], ComputeCommandQueueFlags.None);
            prog = new ComputeProgram(ctx, kernels);
            prog.Build(null, null, null, IntPtr.Zero);
            gpu_lx = new ComputeBuffer<int>(ctx, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, new int[1] { data.size[0] });
            gpu_ly = new ComputeBuffer<int>(ctx, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, new int[1] { data.size[1] });
            gpu_u0 = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, new double[1] { data.u0 });
            gpu_number = new ComputeBuffer<int>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, new int[1]);
            gpu_number9 = new ComputeBuffer<int>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, new int[1]);
            gpu_omega = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, new double[1] { (1d / data.tau) });
            gpu_lower = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, new double[1] { data.inletID[0]/data.size[0] });
            gpu_upper = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, new double[1] { data.inletID[1]/data.size[0] });
            gpu_map = new ComputeBuffer<int>(ctx, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, data.map);
            gpu_u = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, data.ux);
            gpu_v = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, data.uy);
            gpu_density = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, data.density);
            gpu_fin = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, data.fin);
            gpu_fout = new ComputeBuffer<double>(ctx, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.CopyHostPointer, data.fout);

            worker = new long[] { (long)(data.size[0] * data.size[1]) };
            worker9 = new long[] { (long)(data.size[0] * data.size[1] * 9) };
        }

        private void compileKernel()
        {
            int gridSize = data.size[0] * data.size[1];

            cq.Write<int>(gpu_number, new int[] { gridSize }, null);
            cq.Write<int>(gpu_number9, new int[] { gridSize * 9 }, null);

            kernelCollision = prog.CreateKernel("collision");
            kernelStream = prog.CreateKernel("stream");
            kernelSwap = prog.CreateKernel("swap");
            kernelBounceback = prog.CreateKernel("bounceback");
            kernelBC = prog.CreateKernel("boundary");
            kernelMacro = prog.CreateKernel("macroscopic");

            kernelCollision.SetMemoryArgument(0, gpu_map);
            kernelCollision.SetMemoryArgument(1, gpu_fin);
            kernelCollision.SetMemoryArgument(2, gpu_u);
            kernelCollision.SetMemoryArgument(3, gpu_v);
            kernelCollision.SetMemoryArgument(4, gpu_omega);
            kernelCollision.SetMemoryArgument(5, gpu_density);
            kernelCollision.SetMemoryArgument(6, gpu_fout);
            kernelCollision.SetMemoryArgument(7, gpu_number9);

            kernelStream.SetMemoryArgument(0, gpu_fout);
            kernelStream.SetMemoryArgument(1, gpu_fin);
            kernelStream.SetMemoryArgument(2, gpu_lx);
            kernelStream.SetMemoryArgument(3, gpu_ly);
            kernelStream.SetMemoryArgument(4, gpu_map);
            kernelStream.SetMemoryArgument(5, gpu_number9);

            kernelSwap.SetMemoryArgument(0, gpu_fin);
            kernelSwap.SetMemoryArgument(1, gpu_fout);
            kernelSwap.SetMemoryArgument(2, gpu_map);
            kernelSwap.SetMemoryArgument(3, gpu_number9);

            kernelBounceback.SetMemoryArgument(0, gpu_fout);
            kernelBounceback.SetMemoryArgument(1, gpu_fin);
            kernelBounceback.SetMemoryArgument(2, gpu_lx);
            kernelBounceback.SetMemoryArgument(3, gpu_ly);
            kernelBounceback.SetMemoryArgument(4, gpu_map);
            kernelBounceback.SetMemoryArgument(5, gpu_number9);

            kernelBC.SetMemoryArgument(0, gpu_fin);
            kernelBC.SetMemoryArgument(1, gpu_lx);
            kernelBC.SetMemoryArgument(2, gpu_lower);
            kernelBC.SetMemoryArgument(3, gpu_upper);
            kernelBC.SetMemoryArgument(4, gpu_map);
            kernelBC.SetMemoryArgument(5, gpu_u0);
            kernelBC.SetMemoryArgument(6, gpu_number);

            kernelMacro.SetMemoryArgument(0, gpu_fin);
            kernelMacro.SetMemoryArgument(1, gpu_density);
            kernelMacro.SetMemoryArgument(2, gpu_u);
            kernelMacro.SetMemoryArgument(3, gpu_v);
            kernelMacro.SetMemoryArgument(4, gpu_map);
            kernelMacro.SetMemoryArgument(5, gpu_number);
        }

        public void createKernels()
        {
            // set the kernels
            kernels = @"__kernel void collision(
                        global read_only int* map, 
                        global read_only double* fin, 
                        global read_only double* u, 
                        global read_only double* v, 
                        global read_only double* omega,
                        global read_only double* density, 
                        global write_only double* fout, 
                        global read_only int* N
) 
{
    int tid = get_global_id(0);
    if(tid < N[0]) {
        int grid = tid / 9;

        double ex[] = {0, 1, 0, -1, 0, 1, -1, -1, 1};
        double ey[] = {0, 0, 1, 0, -1, 1, 1, -1, -1};
        double weight[] = {4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
        
        if(map[grid] != 1) {
            double eu, usr;
            int n = tid % 9;
            double feq = 0;
            
            eu = u[grid] * ex[n] + v[grid] * ey[n];
            usr = u[grid] * u[grid] + v[grid] * v[grid];
            feq = weight[n] * density[grid] * (1 + 3*eu + 4.5*eu*eu - 1.5*usr);
            fout[tid] = fin[tid] - omega[0] * (fin[tid] - feq);
        }
    }
}

__kernel void stream(
                    global read_only double* fout, 
                    global write_only double* fin, 
                    global read_only int* lx, 
                    global read_only int* ly, 
                    global read_only int* map, 
                    global read_only int* N
) {
    int tid = get_global_id(0);
    if(tid < N[0]) {
        int grid = tid / 9;
        int n = tid % 9;
        int gx, gy, dx, dy, nx, ny, dt;

        double ex[] = {0, 1, 0, -1, 0, 1, -1, -1, 1};
        double ey[] = {0, 0, 1, 0, -1, 1, 1, -1, -1};

        if(map[grid] != 1) {
            nx = lx[0];
            ny = ly[0];
            gx = grid % nx + ex[n];
            gy = grid / nx + ey[n];
            dx = gx % nx + nx;
            dy = gy % ny + ny;
            dx = dx % nx;
            dy = dy % ny;
            dt = 9*(dx + nx * dy) + n;

            fin[dt] = fout[tid];
        }
    }
}

__kernel void swap(
                    global read_only double* fin, 
                    global write_only double* fout, 
                    global read_only int* map, 
                    global read_only int* N
)
{
    int tid = get_global_id(0);
    if(tid < N[0]) {
        int grid = tid / 9;

        if(map[grid] == 1) {
            int sindex[] = {0, 2, 2,-2, -2, 2, 2, -2, -2};
            int n = tid % 9;
            fout[tid + sindex[n]] = fin[tid];
        }
    }
}

__kernel void bounceback(
                        global read_only double* fout, 
                        global write_only double* fin, 
                        global read_only read_only int* nx, 
                        global read_only read_only int* ny,
                        global read_only int* map, 
                        global read_only int* N
)
{
    int tid = get_global_id(0);
    if(tid < N[0]) {
        int lx, ly, grid, n;
        lx = nx[0];
        ly = ny[0];
        grid = tid/9;
        n = tid%9;
        
        if (map[grid] == 1) {
            float ex[] = {0, 1, 0, -1, 0, 1, -1, -1, 1};
            float ey[] = {0, 0, 1, 0, -1, 1, 1, -1, -1};

            int gx, gy, dx, dy;
            gx = grid % lx + ex[n];
            gy = grid / lx + ey[n];
            dx = gx % lx + lx;
            dy = gy % ly + ly;
            dx = dx % lx;
            dy = dy % ly;

            fin[9 * (dx + lx * dy) + n] = fout[tid];
        }
    }
}

__kernel void boundary(
                        global double* fin, 
                        global read_only read_only int* nx, 
                        global read_only double* lower,
                        global read_only double* upper,
                        global read_only int* map, 
                        global read_only double* u0, 
                        global read_only int* N
)
{
    int tid = get_global_id(0);
    if(tid < N[0]) {
        int lx = nx[0];
        double a = lower[0];
        double b = upper[0];
        double j = tid / lx;
        double uu = u0[0];
        double ux = 0;
        double density = 0;
        
        if (map[tid] == 2) {
            ux = -4.0 * uu / ((a - b)*(a - b)) * (j - a) * (j - b);
            density = (fin[9 * tid + 0] + fin[9 * tid + 2] + fin[9 * tid + 4] + 2.0 * (fin[9 * tid + 3] + fin[9 * tid + 7] + fin[9 * tid + 6])) / (1.0 - ux);

            fin[9 * tid + 1] = fin[9 * tid + 3] + density * ux * (2.0 / 3.0);
            fin[9 * tid + 5] = fin[9 * tid + 7] - (fin[9 * tid + 2] - fin[9 * tid + 4]) / 2.0 + density * ux / 6.0;
            fin[9 * tid + 8] = fin[9 * tid + 6] + (fin[9 * tid + 2] - fin[9 * tid + 4]) / 2.0 + density * ux / 6.0;
        }

        if (map[tid] == 3) {
            ux = 4.0 * uu / ((a - b)*(a - b)) * (j - a) * (j - b);
            density = (fin[9 * tid + 0] + fin[9 * tid + 2] + fin[9 * tid + 4] + 2.0 * (fin[9 * tid + 1] + fin[9 * tid + 5] + fin[9 * tid + 8])) / (1.0 + ux);

            fin[9 * tid + 3] = fin[9 * tid + 1] - density * ux * (2.0 / 3.0);
            fin[9 * tid + 7] = fin[9 * tid + 5] + (fin[9 * tid + 2] - fin[9 * tid + 4]) / 2.0 - density * ux / 6.0;
            fin[9 * tid + 6] = fin[9 * tid + 8] - (fin[9 * tid + 2] - fin[9 * tid + 4]) / 2.0 - density * ux / 6.0;
        }

        if (map[tid] == 4) {
            fin[9 * tid + 2] = fin[2 + 9 * (tid + lx)];
            fin[9 * tid + 5] = fin[5 + 9 * (tid + lx)];
            fin[9 * tid + 6] = fin[6 + 9 * (tid + lx)];
        }
    }
}

__kernel void macroscopic(
                            global double* fin, 
                            global write_only double* density, 
                            global write_only double* u, 
                            global write_only double* v, 
                            global read_only int* map,
                            global read_only int* N)
{
    int tid = get_global_id(0);
    if(tid < N[0]) {
        double ex[] = {0, 1.0, 0, -1.0, 0, 1.0, -1.0, -1.0, 1.0};
        double ey[] = {0, 0, 1.0, 0, -1.0, 1.0, 1.0, -1.0, -1.0};
        double buffer = 0.0;
        double buffer_u = 0.0;
        double buffer_v = 0.0;
        
        if(map[tid] != 1) {
            for(int n = 0 ; n < 9 ; n++) {
	            buffer += fin[9 * tid + n];
                buffer_u += ex[n] * fin[9 * tid + n];
                buffer_v += ey[n] * fin[9 * tid + n];
            }

            density[tid] = buffer;
            u[tid] = buffer_u / buffer;
            v[tid] = buffer_v / buffer;
        }
    }
}";
        }

        public void run()
        {
            // actuators
            cq.Execute(kernelCollision, null, worker9, null, null);
            cq.Execute(kernelStream, null, worker9, null, null);
            cq.Execute(kernelSwap, null, worker9, null, null);
            cq.Execute(kernelBounceback, null, worker9, null, null);
            cq.Execute(kernelBC, null, worker, null, null);
            cq.Execute(kernelMacro, null, worker, null, null);
            cq.Finish();

            data.ux = cq.Read<double>(gpu_u, null);
            data.uy = cq.Read<double>(gpu_v, null);
            data.density = cq.Read<double>(gpu_density, null);
        }
    }
}