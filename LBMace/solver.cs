﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LBMace
{
    class Solver
    {
        Setup data;
        private double[] weight, ex, ey;
        private int[] opp;
        private int index, i, j;

        public Solver()
        {
            weight = new double[9] { 4d / 9d, 1d / 9d, 1d / 9d, 1d / 9d, 1d / 9d, 1d / 36d, 1d / 36d, 1d / 36d, 1d / 36d };
            ex = new double[9] { 0, 1d, 0, -1d, 0, 1d, -1d, -1d, 1d };
            ey = new double[9] { 0, 0, 1d, 0, -1d, 1d, 1d, -1d, -1d };
            opp = new int[9] { 0, 3, 4, 1, 2, 7, 8, 5, 6 };
            data = Setup.get();
        }

        public void collision()
        {
            double eu, eusqr, usqr, feq;

            for (int index = 0; index < data.size[0] * data.size[1]; index++)
            {
                if (data.map[index] != 1)
                {
                    usqr = Math.Pow(data.ux[index], 2) + Math.Pow(data.uy[index], 2);
                    for (int n = 0; n < 9; n++)
                    {
                        eu = ex[n] * data.ux[index] + ey[n] * data.uy[index];
                        eusqr = eu * eu;
                        feq = weight[n] * data.density[index] * (1d + 3d * eu + 4.5d * eusqr - 1.5d * usqr);

                        data.fout[n + 9 * index] = data.fin[n + 9 * index] - (1d/data.tau) * (data.fin[n + 9 * index] - feq);
                    }
                }
            }
        }

        public void streaming(double[] input, double[] output, int mode)
        {
            int ax, ay, bx, by;

            for (index = 0; index < data.size[0] * data.size[1]; index++)
            {
                i = index % data.size[0];
                j = index / data.size[0];

                if (isWall(mode))
                {
                    for (int n = 0; n < 9; n++)
                    {
                        ax = i + (int)ex[n];
                        ay = j + (int)ey[n];

                        bx = ((ax % data.size[0]) + data.size[0]) % data.size[0];
                        by = ((ay % data.size[1]) + data.size[1]) % data.size[1];

                        output[n + 9 * (bx + data.size[0] * by)] = input[n + 9 * index];
                    }
                }
            }
        }

        private bool isWall(int mode)
        {
            bool output = true;

            switch (mode)
            {
                case 0:
                    if (data.map[index] != 1)
                    {
                        output = true;
                    }
                    else
                    {
                        output = false;
                    }
                    break;

                case 1:
                    if (data.map[index] == 1)
                    {
                        output = true;
                    }
                    else
                    {
                        output = false;
                    }
                    break;
            }
            return output;
        }

        public void bounceback()
        {
            for (int index = 0; index < data.size[0] * data.size[1]; index++)
            {
                if (data.map[index] == 1)
                {
                    for (int n = 0; n < 9; n++)
                    {
                        data.fout[opp[n] + 9 * index] = data.fin[n + 9 * index];
                    }
                }
            }

            streaming(data.fout, data.fin, 1);
        }

        public void boundary()
        {
            for (index = 0; index < data.size[0] * data.size[1]; index++)
            {
                switch (data.map[index])
                {
                    case 2://inlet left
                        inletLeft();
                        break;

                    case 3://inlet right
                        inletRight();
                        break;

                    case 4://outlet bottom
                        outletOpen();
                        break;
                }
            }
        }

        private void inletLeft()
        {
            int index9 = 9 * index;

            data.ux[index] = (-4.0d * 1.5d * data.u0 / Math.Pow((data.inletID[0]/data.size[0] - data.inletID[1]/data.size[0]), 2)) * (index / data.size[0] - data.inletID[0]/data.size[0]) * (index / data.size[0] - data.inletID[1]/data.size[0]);
            data.uy[index] = 0;

            data.density[index] = (data.fin[0 + index9] + data.fin[2 + index9] + data.fin[4 + index9] + 2d * (data.fin[3 + index9] + data.fin[6 + index9] + data.fin[7 + index9])) / (1 - data.ux[index]);

            data.fin[1 + index9] = data.fin[3 + index9] + (2d / 3d) * data.density[index] * data.ux[index];
            data.fin[5 + index9] = data.fin[7 + index9] - (data.fin[2 + index9] - data.fin[4 + index9]) / 2d + (1d / 6d) * data.density[index] * data.ux[index];
            data.fin[8 + index9] = data.fin[6 + index9] + (data.fin[2 + index9] - data.fin[4 + index9]) / 2d + (1d / 6d) * data.density[index] * data.ux[index];
        }

        private void inletRight()
        {
            int index9 = 9 * index;

            data.ux[index] = (4.0d * 1.5d * data.u0 / Math.Pow((data.inletID[0] / data.size[0] - data.inletID[1] / data.size[0]), 2)) * (index / data.size[0] - data.inletID[0] / data.size[0]) * (index / data.size[0] - data.inletID[1] / data.size[0]);
            data.uy[index] = 0;

            data.density[index] = (data.fin[0 + index9] + data.fin[2 + index9] + data.fin[4 + index9] + 2d * (data.fin[1 + index9] + data.fin[5 + index9] + data.fin[8 + index9])) / (1 + data.ux[index]);

            data.fin[3 + index9] = data.fin[1 + index9] - (2d / 3d) * data.density[index] * data.ux[index];
            data.fin[7 + index9] = data.fin[5 + index9] + (data.fin[2 + index9] - data.fin[4 + index9]) / 2d - (1d / 6d) * data.density[index] * data.ux[index];
            data.fin[6 + index9] = data.fin[8 + index9] - (data.fin[2 + index9] - data.fin[4 + index9]) / 2d - (1d / 6d) * data.density[index] * data.ux[index];
        }

        private void outletOpen()
        {
            int index9 = 9 * index;

            data.fin[2 + index9] = data.fin[2 + 9 * (index + data.size[0] * 1)];
            data.fin[5 + index9] = data.fin[5 + 9 * (index + data.size[0] * 1)];
            data.fin[6 + index9] = data.fin[6 + 9 * (index + data.size[0] * 1)];
        }

        public void macroscopic()
        {
            for(index = 0; index < data.size[0] * data.size[1];index++)
            {
                if (data.map[index] != 1)
                {
                    data.density[index] = 0;
                    data.ux[index] = 0;
                    data.uy[index] = 0;

                    for (int n = 0; n < 9; n++)
                    {
                        data.density[index] += data.fin[n + 9 * index];
                        data.ux[index] += ex[n] * data.fin[n + 9 * index];
                        data.uy[index] += ey[n] * data.fin[n + 9 * index];
                    }

                    data.ux[index] /= data.density[index];
                    data.uy[index] /= data.density[index];
                }
            }
        }
    }
}
