﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LBMace
{
    class Postprocess
    {
        Setup data;
        public int iter;
        string filepath, filename, name;

        public Postprocess()
        {
            iter = 0;
        }

        public void setMeta(string path, string name)
        {
            data = Setup.get();
            iter = 0;
            filepath = path;
            filename = name;
        }

        private string nameFiles(string mode)
        {
            string output = "";

            output = filepath + filename + iter + "." + mode;
            return output;
        }

        public void saveFiles()
        {
            StringBuilder post = new StringBuilder();

            post.Append("# vtk DataFile Version 3.0");
            post.AppendLine();
            post.Append("fluid_state");
            post.AppendLine();
            post.Append("ASCII");
            post.AppendLine();
            post.Append("DATASET STRUCTURED_POINTS");
            post.AppendLine();
            post.Append("DIMENSIONS " + data.size[0].ToString() + " " + data.size[1].ToString() + " 1");
            post.AppendLine();

            post.Append("ORIGIN 0 0 0");
            post.AppendLine();
            post.Append("SPACING 1 1 1");
            post.AppendLine();

            post.Append("POINT_DATA " + (data.size[0] * data.size[1]).ToString());
            post.AppendLine();
            post.Append("SCALARS density double 1");
            post.AppendLine();
            post.Append("LOOKUP_TABLE default");
            post.AppendLine();

            for (int index = 0; index < data.size[0] * data.size[1]; index++)
            {
                post.Append(data.density[index].ToString());
                post.AppendLine();
            }

            post.Append("VECTORS velocity_vector double");
            post.AppendLine();
            for (int index = 0; index < data.size[0] * data.size[1]; index++)
            {
                post.Append(data.ux[index].ToString() + " " + data.uy[index].ToString() + " 0");
                post.AppendLine();
            }

            name = nameFiles("vtk");
            System.IO.File.WriteAllText(name, post.ToString());
        }

        public void saveImages()
        {
            Bitmap buffer = new Bitmap(data.size[0], data.size[1]);
            int i, j;

            for (int index = 0; index < data.size[0] * data.size[1]; index++)
            {
                i = index % data.size[0];
                j = index / data.size[0];

                switch (data.map[index])
                {
                    case 0:
                        buffer.SetPixel(i, j, Color.White);
                        break;
                    case 1:
                        buffer.SetPixel(i, j, Color.Black);
                        break;
                    case 2:
                        buffer.SetPixel(i, j, Color.Red);
                        break;
                    case 3:
                        buffer.SetPixel(i, j, Color.Blue);
                        break;
                    case 4:
                        buffer.SetPixel(i, j, Color.Green);
                        break;
                }
            }

            name = nameFiles("bmp");
            buffer.Save(name, System.Drawing.Imaging.ImageFormat.Bmp);
        }
    }
}
