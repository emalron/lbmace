﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace LBMace
{
    public partial class Form1 : Form
    {
        Setup setup;
        Solver solver;
        Postprocess postprocess;
        GPGPU gpu;

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            init();
        }

        public Form1()
        {
            InitializeComponent();
        }

        void init()
        {
            setup = Setup.get();
            solver = new Solver();
            postprocess = new Postprocess();
            gpu = new GPGPU();

            setup.mapping();
            drawTiles();
            setVelocity();
            postprocess.setMeta(textBox1.Text, textBox2.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = false;

            for (postprocess.iter = 0; postprocess.iter < (int)numericIter.Value; postprocess.iter++)
            {
                simulate();
                if (postprocess.iter % 10 == 0)
                {
                    post();
                }
            }
        }

        private void simulate()
        {
            solver.macroscopic();
            solver.collision();
            solver.streaming(setup.fout, setup.fin, 0);
            solver.bounceback();
            solver.boundary();
        }

        private void post()
        {
            postprocess.saveFiles();
            //postprocess.saveImages();
        }

        private void tb_u0_TextChanged(object sender, EventArgs e)
        {
            setVelocity();
        }

        private void tb_Re_TextChanged(object sender, EventArgs e)
        {
            setVelocity();
        }

        private void drawTiles()
        {
            Bitmap buffer = new Bitmap(setup.size[0], setup.size[1]);
            int i, j;

            for (int index = 0; index < setup.size[0] * setup.size[1]; index++)
            {
                i = index % setup.size[0];
                j = index / setup.size[0];

                switch (setup.map[index])
                {
                    case 0:
                        buffer.SetPixel(i, j, Color.White);
                        break;
                    case 1:
                        buffer.SetPixel(i, j, Color.Black);
                        break;
                    case 2:
                        buffer.SetPixel(i, j, Color.Red);
                        break;
                    case 3:
                        buffer.SetPixel(i, j, Color.Blue);
                        break;
                    case 4:
                        buffer.SetPixel(i, j, Color.Green);
                        break;
                }
            }

            buffer.RotateFlip(RotateFlipType.RotateNoneFlipY);

            pictureBox1.Width = setup.size[0];
            pictureBox1.Height = setup.size[1];
            pictureBox1.Image = buffer;

            label5.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + pictureBox1.Height + 10);
            label5.Text = "the size is " + setup.size[0].ToString() + "x" + setup.size[1].ToString();
        }

        private void setVelocity()
        {
            setup.Re = Convert.ToDouble(tb_Re.Text);
            setup.tau = Convert.ToDouble(tb_relaxT.Text);
            setup.relaxationTime(setup.Re, setup.tau);
                        
            tb_u0.Text = setup.u0.ToString();
            tb_Ma.Text = (setup.u0 * Math.Sqrt(3)).ToString();
        }

        private void tb_relaxT_TextChanged(object sender, EventArgs e)
        {
            setVelocity();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            gpu.init();

            for (postprocess.iter = 0; postprocess.iter < (int)numericIter.Value; postprocess.iter++)
            {
                gpu.run();
                if (postprocess.iter % 10    == 0)
                {
                    post();
                }
            }
        }
    }
}
